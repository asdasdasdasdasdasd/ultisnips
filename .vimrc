set nocompatible " Use vim settings, not vi

filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

Bundle 'majutsushi/tagbar' 
Bundle 'tpope/vim-markdown' 
Bundle 'Lokaltog/vim-powerline' 
Bundle 'godlygeek/tabular'

" snipmate dependencies
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'
Bundle 'abroder/snipmate-snippets'
" textmate-style snippets
Bundle 'garbas/vim-snipmate'

filetype plugin on
filetype indent on

syntax on " Turn on syntax highlighting

" GENERAL
let mapleader=','
nmap ; :
inoremap jk <Esc>

set encoding=utf-8
set hidden                     " Makes vim buffers work
set number                     " Turn on line numbers
set backspace=indent,eol,start " Allow backspace in insert mode
set showmode                   " Show current mode at bottom
set showcmd
set autoread                   " Reload files changed outside vim
set wildmenu
set wildmode=list:longest

" STATUS LINE
set laststatus=2                  " Always show status bar
let g:Powerline_symbols = 'fancy'
set ruler                         " Show line, position in status bar

set undofile           " Create persistent undo files
set undodir=~/.vim/tmp " Save them in a single folder


let g:netrw_browse_split=2 " Set netrw to open files in vsplit
let g:netrw_liststyle=3    " Set netrw to default to tree view
let g:netrw_altv=1         " Open vsplits on the right, not left

" SEARCH

set incsearch  " Find match as you type
set hlsearch   " Highlight search by default
set ignorecase " Case insensitive search...
set smartcase  " ...unless there's a capital letter
set gdefault   " Replace every instance in a line, not the first

" INDENTATION

set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

" Reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

autocmd FileType make setlocal noexpandtab " Turns off autoexpand in Makefiles

" SCROLLING

set scrolloff=8
set sidescrolloff=15
set sidescroll=1

" SHORTCUTS
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
